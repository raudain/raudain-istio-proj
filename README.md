# Terraform EKS and Argo CD Configuration Repository

This repository contains Terraform configurations, Kubernetes manifests, and documentation files for deploying and managing an EKS cluster with Argo CD. Below is a detailed description of each file and its purpose.

## Table of Contents

- [Terraform Hub Configuration Files](#terraform-hub-configuration-files)
- [Argo CD Manifests](#argo-cd-manifests)
- [Documentation](#documentation)
- [Getting Started](#getting-started)
- [Deploying Resources](#deploying-resources) 
- [Contributions](#contributions)
- [License](#license)
- [Argo CD deployment]

## Terraform Hub Configuration Files

There are two seperate folders for the vpc and the cluster. This was mainly done to to avoid conflicts during deployment but may not be necessary. "vpc" is the folder with the vpc's configuration files and "hub" is the folder with the eks clusters deployment files.

### `main.tf`
There are three modules in this file. Eks, gitops_bridge_bootstrap, and eks_blueprints_addons. 

The eks module is provisioning the cluster, managed node groups, auth_configmap, cluster admin role, and the addons eks-pod-identity-agent and vpc-cni. Initially, access to the cluster will be granted through the role that creates the cluster and the cluster admin. This can be reconfigured to taste but I imagine that future roles will be create with RBAC and least privilige.

The gitops_bridge_bootstrap is being used to install Argo CD into the cluster. This will be used for deployment into the cluster via gitops. Although things like helm charts and pods can technically be deployed via Terraform, managing lifecycle rules and drift become tedious and complicated and more micro services are created. Arg CD manages both of those with automatic drift correction and replomyent of recources that go down. All of the Kube manifests live in a seperate repo that is refrenced inside of Argo CD. See "Argo ..." below for mor information. 

It also installs an appofapps. This is necessary to deploy kubernetes manifests through gitops. Without an appofapps you will have to manually apply the applications through kubectl or the ArgoCD UI in order for the resources to function.
This is a simple diagram of how it works:
![Alt text](/assets/images/argocd-gitops.webp)
An application-set.yaml is injected into the cluster during an ```terraform apply``` via "kubernetes_manifest" resource:
![Alt text](/assets/images/kubernetes_manifest-block.png)
That application-set is pointing to a predefined repo, revision, and path. Everytime a k8s manifest is uploaded to that path, argocd will pull it and create it in the cluster. The application set needs the repoURL and basepath in order to know where to pull from:
![Alt text](/assets/images/application-set-repo.png)

Those variables should be defined in locals:
![Alt text](/assets/images/locals-repo-url.png)
![Alt text](/assets/images/localtovar.png)

These local variables are further defined in the variables.tf file. If you are are pointing to a private repo, credentials will be required.



The eks_blueprints_addons is used to provision the addons for the cluster but the manifest for these configuration are stored on a serperate repo.

The modules refrence the local variables which refrence the variable as the source of truth

### `variables.tf`
Defines input variables used in the `main.tf` configuration, allowing parameterization for flexibility and reusability.

### `outputs.tf`
Defines the outputs of the Terraform configuration. Outputs display values such as instance IDs and IP addresses at the end of a `terraform apply` or `terraform plan` operation.

### `versions.tf`
Specifies required versions of Terraform and provider plugins to ensure compatibility and prevent issues from incompatible versions.


## Argo CD Manifests

### `appofapps-applicationset.yaml`
A Kubernetes manifest for an ApplicationSet in Argo CD. This allows managing multiple applications with a single configuration by dynamically generating applications based on a template and parameters. This will be where a lot of the eks "add-ons" will live. The manifest files for Argo CD currently live at: https://github.com/reneaudain/eks-blueprints-for-terraform-workshop.git. This can be changed to a repo of your choice but note the configuration files of the repo.

## Documentation

### `README.md`
Provides an overview of the repository, instructions for getting started, and detailed descriptions of the files.

### `CONTRIBUTING.md`
Guidelines for contributing to the repository, including code standards, commit message conventions, and the process for submitting pull requests.

### `CODE_OF_CONDUCT.md`
The code of conduct that contributors are expected to adhere to, ensuring a welcoming and respectful environment for all.

### `LICENSE`
The license under which the repository is distributed. This repository uses the MIT License.

### `Taskfile.yaml`
A task runner configuration file that defines various automation tasks to simplify development and deployment workflows.

### `contentspec.yaml`
Defines the content specifications for the repository, ensuring that all required files and documentation are included and up to date.

## Getting Started

### Prerequisites
- Terraform installed on your local machine
- Helm installed on your local machine
- Repo for Kube Manifests
- AWS CLI configured with appropriate permissions (if using AWS as the provider)



## Deploying Resources

1. **Clone Repo:**
   ```sh
   git clone https://gitlab.com/raudain/raudain-istio-proj.git
   ```

2. **Create VPC:**
   ```sh
   cd <path>/raudain-istio-proj/environment/vpc
   terraform init
   terraform apply -auto-approve
   ```

3. **Create Cluster:**
   ```sh
   cd <path>/raudain-istio-proj/environment/hub
   terraform init
   terraform apply -target="module.eks" -auto-approve
   ```

4. **Add the cluster context to Kubeconfig:**
   
   ```sh
   aws eks --region <region> update-kubeconfig --name <cluster-name> 
   ```

5. **Apply the rest of the configuration:**
   ```sh
   cd <path>/raudain-istio-proj/environment/hub
   terraform init
   terraform apply -auto-approve
   ```

6. **Configure repo credentials in the shell:**
   GitHub is being used in this example but use the credentials for the account of the repo that you have made the source of truth for Argo CD
   ```sh
   export GITHUB_USER=“Insert-User-Name-Here”
   export GITHUB_LOGIN=$GITHUB_USER
   export GITHUB_TOKEN=<insert-PAT-Token-Here>
   ```

7. **Extract ArgoCD Dashboard URL:**
   Click the link after extraction
   ```sh
   export ARGOCD_SERVER=$(kubectl --context hub get svc argo-cd-argocd-server -n argocd -o json | jq --raw-output '.status.loadBalancer.ingress[0]. hostname')
echo export ARGOCD_SERVER="$ARGOCD_SERVER" >> ~/.bashrc
echo "https://$ARGOCD_SERVER"
   ```

8. **Extract ArgoCD login password:**
   The username is "admin" by default. Be sure to change this once logged in. There is a way to setup SSO in settings.
   ```sh
   ARGOCD_PWD=$(kubectl --context hub -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d)
echo export ARGOCD_PWD="$ARGOCD_PWD" >> ~/.bashrc
echo "Argo CD admin password: $ARGOCD_PWD"
   ```


##  

4. **Deploy the Kubernetes resources:**
   ```sh
   kubectl apply -f appofapps-applicationset.yaml
   ```

4. **Deploy the Kubernetes resources:**
   ```sh
   kubectl apply -f appofapps-applicationset.yaml
   ```

4. **Deploy the Kubernetes resources:**
   ```sh
   kubectl apply -f appofapps-applicationset.yaml
   ```

4. **Deploy the Kubernetes resources:**
   ```sh
   kubectl apply -f appofapps-applicationset.yaml
   ```

### Notes
- Ensure that sensitive information such as AWS credentials is not hard-coded in the Terraform files. Use environment variables or secret management tools instead.
- Regularly update and version control the Terraform state files and backup files.

## Contributions
Please refer to the `CONTRIBUTING.md` file for guidelines on contributing to this repository. We welcome contributions from the community to improve and expand the configurations and documentation.

## License
This repository is licensed under the MIT License. See the `LICENSE` file for more information.

---

By following this README, you should have a comprehensive understanding of what each file in the repository does and how to use them effectively.