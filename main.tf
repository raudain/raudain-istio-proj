/*module "vpc_cni_irsa" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"

  role_name             = "vpc_cni"
  attach_vpc_cni_policy = true
  vpc_cni_enable_ipv4   = true

  oidc_providers = {
    main = {
      provider_arn               = module.eks.oidc_provider_arn
      namespace_service_accounts = ["kube-system:aws-node"]
    }
  }

  tags = {
    Environment = "dev"
    Terraform   = "true"
  }
}*/

module "eks_blueprints_addons" {
  source  = "aws-ia/eks-blueprints-addons/aws"
  version = "1.16.3" #ensure to update this to the latest/desired version

  cluster_name      = module.eks.cluster_name
  cluster_endpoint  = module.eks.cluster_endpoint
  cluster_version   = module.eks.cluster_version
  oidc_provider_arn = module.eks.oidc_provider_arn
  depends_on        = [module.eks]
  
  eks_addons = {
    aws-ebs-csi-driver = {
      most_recent = true
    }
    coredns = {
      most_recent = true
    }
    /*vpc-cni = {
      most_recent = true
      service_account_role_arn = module.vpc_cni_irsa.iam_role_arn

    }*/
    kube-proxy = {
      most_recent = true
    }

    #EBS CSI Driver
    aws-ebs-csi-driver = {
      most_recent = true
    }

    #EFS CSI Driver
    aws-efs-csi-driver = {
      most_recent = true
    }

    #S3 CSI Driver
    aws-mountpoint-s3-csi-driver = {
      most_recent = true
    }

    #Snapshot Controller
    snapshot-controller = {
      most_recent = true
    }

    #GuardDuty Agent
    #aws-guardduty-agent = {
    #  most_recent = true
    #}    
  }

  enable_aws_load_balancer_controller    = true
  enable_cluster_proportional_autoscaler = true
  enable_karpenter                       = false
  enable_kube_prometheus_stack           = false
  enable_metrics_server                  = true
  enable_external_dns                    = true
  enable_external_secrets                = false
  #enable_cert_manager                    = true
  #cert_manager_route53_hosted_zone_arns  = ["arn:aws:route53:::hostedzone/XXXXXXXXXXXXX"]

  /*helm_releases = {
    prometheus-adapter = {
      description      = "A Helm chart for k8s prometheus adapter"
      namespace        = "prometheus-adapter"
      create_namespace = true
      chart            = "prometheus-adapter"
      chart_version    = "4.2.0"
      repository       = "https://prometheus-community.github.io/helm-charts"
      values = [
        <<-EOT
          replicas: 2
          podDisruptionBudget:
            enabled: true
        EOT
      ]
    }

  }*/

  tags = {
    Environment = "dev"
  }
}


#create a managed eks cluster. the cluster should be in private subnets with public access. the cluster should use the aws vpc module in the vpc.tf file. the fluster should have theses add-on: the csi add-on for ebs, IAM OIDC provider, DataDog add-on, GitLab CI Integration, aws-efs-csi-driver, aws-mountpoint-s3-csi-driver, snapshot-controller, and ws-guardduty-agent. the cluster should be able to autoscale as well
module "eks" {
  source                                   = "terraform-aws-modules/eks/aws"
  version                                  = "20.13.1"
  cluster_name                             = local.cluster_name
  cluster_version                          = "1.29"
  subnet_ids                               = module.vpc.private_subnets
  enable_cluster_creator_admin_permissions = true
  cluster_endpoint_public_access           = true
  #cluster_endpoint_private_access = false

  

  eks_managed_node_group_defaults = {
    instance_types = ["t3.small"]
    ami_type       = "AL2_x86_64"
  }
  
  /*node_security_group_additional_rules = {
    ingress_15017 = {
      description                   = "Cluster API - Istio Webhook namespace.sidecar-injector.istio.io"
      protocol                      = "TCP"
      from_port                     = 15017
      to_port                       = 15017
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_15012 = {
      description                   = "XDS and CA services - TLS (istio)"
      protocol                      = "TCP"
      from_port                     = 15012
      to_port                       = 15012
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_15014 = {
      description                   = "Control plane monitoring for istiod"
      protocol                      = "TCP"
      from_port                     = 15014
      to_port                       = 15014
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_15443 = {
      description                   = "SNI (istio)"
      protocol                      = "TCP"
      from_port                     = 15443
      to_port                       = 15443
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_15000 = {
      description                   = "Envoy admin port for commands/diagnostics(istio)"
      protocol                      = "TCP"
      from_port                     = 15000
      to_port                       = 15000
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_15001 = {
      description                   = "Envoy Outbound(istio)"
      protocol                      = "TCP"
      from_port                     = 15001
      to_port                       = 15001
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_15006 = {
      description                   = "Envoy Inbound(istio)"
      protocol                      = "TCP"
      from_port                     = 15006
      to_port                       = 15006
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_15020 = {
      description                   = "Istio agent Prometheus telemetry (istio)"
      protocol                      = "TCP"
      from_port                     = 15020
      to_port                       = 15020
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_15021 = {
      description                   = "Health checks(istio)"
      protocol                      = "TCP"
      from_port                     = 15021
      to_port                       = 15021
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_15090 = {
      description                   = "Envoy Prometheus telemetry for istiod(istio)"
      protocol                      = "TCP"
      from_port                     = 15090
      to_port                       = 15090
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_15010 = {
      description                   = "XDS and CA services for istiod (istio)"
      protocol                      = "TCP"
      from_port                     = 15010
      to_port                       = 15010
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_8080 = {
      description                   = "Debug interface (istio)"
      protocol                      = "TCP"
      from_port                     = 8080
      to_port                       = 8080
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_15010 = {
      description                   = "XDS and CA services for istiod (istio)"
      protocol                      = "TCP"
      from_port                     = 15010
      to_port                       = 15010
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_15014 = {
      description                   = "Control plane monitoring (istio)"
      protocol                      = "TCP"
      from_port                     = 15014
      to_port                       = 15014
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_15443 = {
      description                   = "SNI for Ingress and Egress Gateways (istio)"
      protocol                      = "TCP"
      from_port                     = 15443
      to_port                       = 15443
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_15010 = {
      description                   = "XDS and CA services for istiod (istio)"
      protocol                      = "TCP"
      from_port                     = 15010
      to_port                       = 15010
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_9090 = {
      description                   = "Prometheus"
      protocol                      = "TCP"
      from_port                     = 9090
      to_port                       = 9090
      type                          = "ingress"
      source_cluster_security_group = true
    }

    ingress_42422 = {
      description                   = "Telemetry - Prometheus (istio)"
      protocol                      = "TCP"
      from_port                     = 42422
      to_port                       = 42422
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_15010 = {
      description                   = "XDS and CA services for istiod (istio)"
      protocol                      = "TCP"
      from_port                     = 15010
      to_port                       = 15010
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_15004 = {
      description                   = "Policy/Telemetry - mTLS (istio)"
      protocol                      = "TCP"
      from_port                     = 15004
      to_port                       = 15004
      type                          = "ingress"
      source_cluster_security_group = true
    }
    ingress_9091 = {
      description                   = "Policy/Telemetry (istio)"
      protocol                      = "TCP"
      from_port                     = 9091
      to_port                       = 9091
      type                          = "ingress"
      source_cluster_security_group = true
    }

    egress_all = {
      description      = "Egress all traffic to cluster security"
      protocol         = "-1"
      from_port        = 0
      to_port          = 65000
      type             = "egress"
      source_cluster_security_group = true
    }
  }*/




  depends_on = [module.vpc]
  # cluster_iam_role_name     = "your_eks_cluster_iam_role_name"
  # cluster_iam_role_arn      = "arn:aws:iam::YOUR_AWS_ACCOUNT_ID:role/your_eks_cluster_iam_role_name"

  vpc_id = module.vpc.vpc_id

  eks_managed_node_groups = {
    #iam_role_attach_cni_policy = true
    initial = {
      instance_types = ["t3.small"]
      min_size       = 1
      max_size       = 5
      desired_size   = 2

      # Use Spot Instances
      instance_distribution = {
        on_demand_base_capacity                  = 0
        on_demand_percentage_above_base_capacity = 0
        spot_instance_pools                      = 2
      }

      # Optional: Configure Spot Instance options
      spot_instance_distribution = {
        max_price = var.max_price # Maximum price in USD for Spot Instances
      }
    }
  }
}


data "aws_eks_cluster" "cluster" {
  name       = module.eks.cluster_name
  depends_on = [module.eks]
}

data "aws_eks_cluster_auth" "cluster" {
  name       = module.eks.cluster_name
  depends_on = [module.eks]
}


